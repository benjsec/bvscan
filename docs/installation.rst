============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install bvscan
    $ pip install bvscan

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv bvscan
    $ pip install bvscan
