"""
Tests for `bvscan` module.
"""
import logging

import pytest
import requests
from bs4 import BeautifulSoup as bs
from requests.compat import urljoin

from bvscan import bvscan


def setup_dvwa(base_url):
    def get_login_token(raw_resp):
        soup = bs(raw_resp.text, 'html.parser')
        token = [n['value'] for n in soup.find_all('input')
                 if n['name'] == 'user_token']
        return token[0]

    with requests.session() as s:
        resp = s.get(base_url)
        token = get_login_token(resp)

        # Create database
        setup_url = requests.compat.urljoin(base_url, 'setup.php')
        payload = {
            'create_db': 'Create / Reset Database',
            'user_token': token,
        }
        s.post(setup_url, data=payload)

        # Login
        login_url = requests.compat.urljoin(base_url, 'login.php')
        resp = s.get(login_url)
        token = get_login_token(resp)
        payload = {
            'username': 'admin',
            'password': 'password',
            'user_token': token,
            'Login': 'Login',
        }
        s.post(login_url, data=payload)

        # Set security level
        payload = {
            'security': 'low',
            'seclev_submit': 'Submit',
            'user_token': token,
        }
        security_url = requests.compat.urljoin(base_url, 'security.php')
        s.post(security_url, data=payload)
        return s


class TestBvscan(object):

    @pytest.fixture(autouse=True)
    def base_url(self):
        return 'http://localhost'

    # @pytest.fixture(scope='session', autouse=True)
    @pytest.fixture
    def dvwa(self):
        return setup_dvwa('http://localhost')

    def test_seclevel(self, dvwa, base_url):
        """Make sure the DVWA was set up correctly."""
        assert "<em>Security Level:</em> low" in dvwa.get(base_url).text

    # @pytest.mark.skipif(True, reason="Slow")
    def test_find_links(self, dvwa, base_url):
        B = bvscan.Site(base_url=urljoin(base_url, 'index.php'), session=dvwa)
        B.crawl_pages()
        print(B.pages.keys())
        assert len(B.pages) > 25
        assert all([l.startswith('http://localhost') for l in B.pages.keys()])

    def test_form(self, dvwa, base_url):
        P = bvscan.Page(url=urljoin(base_url, 'setup.php'), session=dvwa)
        assert any([i.name == 'create_db'
                    for f in P.forms
                    for i in f.parameters])

    def test_attack(self, dvwa, base_url):
        p = bvscan.Page(
            url=urljoin(base_url, 'vulnerabilities/sqli/'),
            session=dvwa)
        f = list(p.forms)[0]
        assert bvscan.Attacker.try_attack(f, "' OR '1=1")

    def test_attack_fail(self, dvwa, base_url):
        p = bvscan.Page(
            url=urljoin(base_url, 'vulnerabilities/sqli/'),
            session=dvwa)
        f = list(p.forms)[0]
        assert not bvscan.Attacker.try_attack(f, "")

    def test_form_attack(self, dvwa, base_url):
        p = bvscan.Page(
            url=urljoin(base_url, 'vulnerabilities/sqli/'),
            session=dvwa)
        f = list(p.forms)[0]
        A = bvscan.Attacker('site')
        assert A.test_form(f)

    # @pytest.mark.skipif(True, reason="Not ready")
    def test_full(self, dvwa, base_url, caplog):
        # Shouldn't really be part of test suite, but saves having to login.
        caplog.set_level(logging.INFO)
        bvscan.main(base_url, dvwa)
        assert "VULNERABLE" in caplog.text
        # assert False  # So we can see output!
